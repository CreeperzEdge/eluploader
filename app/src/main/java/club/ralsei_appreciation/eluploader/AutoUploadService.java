package club.ralsei_appreciation.eluploader;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.File;

import br.com.onimur.handlepathoz.HandlePathOz;
import br.com.onimur.handlepathoz.HandlePathOzListener;
import br.com.onimur.handlepathoz.model.PathOz;
import retrofit2.Call;

public class AutoUploadService extends Service implements HandlePathOzListener.SingleUri {

    private final ContentObserver observer = new ContentObserver(new Handler()) {
        Uri lastProcessed = null;
        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);

            if (uri.equals(lastProcessed)) return;
            lastProcessed = uri;

            Log.w("O", uri.getPath());
            resolveFileUri(uri);
        }
    };

    private HandlePathOz handlePathOz;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        handlePathOz = new HandlePathOz(this, this);
        getContentResolver().registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, true, observer);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        getContentResolver().unregisterContentObserver(observer);
        if (handlePathOz != null) {
            handlePathOz.deleteTemporaryFiles();
            handlePathOz.onDestroy();
        }
    }

    @Override
    public void onRequestHandlePathOz(PathOz pathOz, Throwable throwable) {
        Log.v("E", pathOz.getPath());
        if (!pathOz.getPath().contains("/Screenshot")) return;
        if (pathOz.getPath().contains(".trashed")) return;
        File file = new File(pathOz.getPath());
        if (file.lastModified() + 60000 < System.currentTimeMillis()) return;
        String token = getSharedPreferences("eluploader-settingsdata", Context.MODE_PRIVATE).getString("token", null);
        if (token == null) return;
        Call<UploadedFile> call = Uploader.makeRequest(pathOz.getPath(), token);
        Uploader.uploadToServer(call, getApplicationContext(), UploadType.BACKGROUND);
    }

    public void resolveFileUri(Uri uri) {
        handlePathOz.getRealPath(uri);
    }
}
