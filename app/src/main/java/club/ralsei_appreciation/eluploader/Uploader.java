package club.ralsei_appreciation.eluploader;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.io.File;
import java.net.URLConnection;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class Uploader {
    public static Call<UploadedFile> makeRequest(String filePath, String token) {
        Retrofit retrofit = RetrofitClientInstance.getRetrofitInstance();

        ElixireService elixire = retrofit.create(ElixireService.class);

        //Create a file object using file path
        File file = new File(filePath);

        String mime = URLConnection.guessContentTypeFromName(file.getName());
        Log.v("E::makeRequest", mime);
        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse(mime), file);

        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("upload", file.getName(), fileReqBody);

        Call<UploadedFile> call = elixire.uploadFile(part, token);

        return call;
    }

    public static <T extends Context> void uploadToServer(Call<UploadedFile> call, final T context, final UploadType origin) {
        Log.v("E", "enqueue");
        call.enqueue(new Callback<UploadedFile>() {
            @Override
            public void onResponse(Call<UploadedFile> call, Response<UploadedFile> response) {
                UploadedFile res = response.body();
                String text = null;
                Boolean success = false;
                if (response.code() != 200 || res == null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        text = jObjError.getString("message");
                    } catch (Throwable e) {
                        text = e.getMessage();
                    }
                } else {
                    text = res.getUrl();
                    success = true;
                    Log.v("E", "done");
                }
                if (origin != UploadType.BACKGROUND) {
                    final AppCompatActivity activityContext = (AppCompatActivity) context;
                    View snack = activityContext.findViewById(R.id.snack);
                    final Snackbar snackbar = Snackbar.make(snack, text, Snackbar.LENGTH_INDEFINITE);
                    final String url = text;
                    if (success) {
                        snackbar.setAction("Copy", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText("link to file", url);
                                clipboard.setPrimaryClip(clip);
                                snackbar.dismiss();
                                Log.v("E::snackbar", context.getClass().getSimpleName());
                                if (origin == UploadType.SHARED) {
                                    activityContext.finish();
                                }
                            }
                        });
                    } else {
                        snackbar.setAction("Dismiss", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                snackbar.dismiss();
                            }
                        });
                    }
                    snackbar.show();
                    activityContext.findViewById(R.id.toolbarprogress).setVisibility(View.INVISIBLE);
                } else {
                    ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("link to file", text);
                    clipboard.setPrimaryClip(clip);
                }
            }

            @Override
            public void onFailure(Call<UploadedFile> call, Throwable t) {
                Log.v("E", "bad");
                t.printStackTrace();
                if (origin != UploadType.BACKGROUND) {
                    final AppCompatActivity activityContext = (AppCompatActivity) context;
                    View snack = activityContext.findViewById(R.id.snack);
                    Snackbar.make(snack, t.getMessage(), Snackbar.LENGTH_LONG).show();
                    activityContext.findViewById(R.id.toolbarprogress).setVisibility(View.INVISIBLE);
                }

            }
        });
    }
}
