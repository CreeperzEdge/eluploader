package club.ralsei_appreciation.eluploader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import br.com.onimur.handlepathoz.HandlePathOz;
import br.com.onimur.handlepathoz.HandlePathOzListener;
import br.com.onimur.handlepathoz.model.PathOz;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity implements HandlePathOzListener.SingleUri {

    private HandlePathOz handlePathOz;
    private UploadType shared = UploadType.DIRECT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPref = getSharedPreferences("eluploader-settingsdata", Context.MODE_PRIVATE);
        String token = sharedPref.getString("token", "");
        boolean autoUpload = sharedPref.getBoolean("autoUpload", false);
        TextView tokenField = findViewById(R.id.tokenField);
        Switch autoUploadSwitch = findViewById(R.id.autoUploadSwitch);
        autoUploadSwitch.setChecked(autoUpload);
        tokenField.setText(token);
        int readfilePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if (readfilePermission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 200);
        }
        handlePathOz = new HandlePathOz(this, this);

        if (autoUpload) {
            getApplicationContext().startService(new Intent(this, AutoUploadService.class));
        }

        Intent receivedIntent = getIntent();
        if (receivedIntent.getType() != null) {
            toggleLoadBar(true);
            Uri receivedUri = (Uri)receivedIntent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (receivedUri.getScheme().equals("content")) {
                shared = UploadType.SHARED;
                handlePathOz.getRealPath(receivedUri);
                return;
            }
            Log.v("S", receivedUri.getPath().replace("/raw/", ""));
            Call<UploadedFile> call = Uploader.makeRequest(receivedUri.getPath().replace("/raw/", ""), token);
            Uploader.uploadToServer(call, this, UploadType.SHARED);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            Uri uri = null;
            if (data != null) {
                toggleLoadBar(true);
                uri = data.getData();
                handlePathOz.getRealPath(uri);
            }
        }
    }

    @Override
    public void onStop() {
        handlePathOz.deleteTemporaryFiles();
        super.onStop();
    }

    public void saveToken(View view) {
        Log.v("E", "event fired");
        TextView tokenField = findViewById(R.id.tokenField);
        String token = tokenField.getText().toString();
        SharedPreferences sharedPref = getSharedPreferences("eluploader-settingsdata", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("token", token);
        editor.commit();
        toggleLoadBar(true);
        Context context = getApplicationContext();

        Log.v("E", "begin parse/upload");
        InputStream in = context.getResources().openRawResource(R.raw.testfile);
        Log.v("E", context.getCacheDir().toString());
        try {
            FileOutputStream out = new FileOutputStream(context.getCacheDir().toString() + "/testfile.webp");
            byte[] buff = new byte[1024];
            int read = 0;
            try {
                while ((read = in.read(buff)) > 0) {
                    out.write(buff, 0, read);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                in.close();
                out.close();
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }

        Call<UploadedFile> call = Uploader.makeRequest(context.getCacheDir().toString() + "/testfile.webp", token);
        Uploader.uploadToServer(call, this, UploadType.DIRECT);

        Log.v("E", "yeah");
    }

    public void configureAutoUpload(View view) {
        SharedPreferences sharedPref = getSharedPreferences("eluploader-settingsdata", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("autoUpload", ((Switch) view).isChecked());
        editor.commit();

        if (((Switch) view).isChecked()) {
            getApplicationContext().startService(new Intent(this, AutoUploadService.class));
        } else {
            getApplicationContext().stopService(new Intent(this, AutoUploadService.class));
        }
    }

    public void resolveFileUri(Uri uri) {
        handlePathOz = new HandlePathOz(this, this);
        handlePathOz.getRealPath(uri);
    }

    public void uploadNewFile(View view) {
        Log.v("E", "fab event fired");
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent,2);
    }

    private void toggleLoadBar(boolean visible) {
        ProgressBar progbar = findViewById(R.id.toolbarprogress);
        if (visible) {
            progbar.setVisibility(View.VISIBLE);
        } else {
            progbar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onRequestHandlePathOz(PathOz pathOz, Throwable throwable) {
        Log.v("E", pathOz.getPath());
        TextView tokenField = findViewById(R.id.tokenField);
        String token = tokenField.getText().toString();
        Call<UploadedFile> call = Uploader.makeRequest(pathOz.getPath(), token);
        Uploader.uploadToServer(call, this, shared);
        shared = UploadType.DIRECT;
    }
}
