package club.ralsei_appreciation.eluploader;

public class UploadedFile {

    private String url;

    private String shortname;

    private String delete_url;

    public UploadedFile(String url, String shortname, String delete_url) {
        this.url = url;
        this.shortname = shortname;
        this.delete_url = delete_url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getDeleteUrl() {
        return delete_url;
    }

    public void setDeleteUrl(String delete_url) {
        this.delete_url = delete_url;
    }

}
