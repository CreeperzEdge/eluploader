package club.ralsei_appreciation.eluploader;

public enum UploadType {
    DIRECT,
    SHARED,
    BACKGROUND
}
